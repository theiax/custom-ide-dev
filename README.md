<a href="https://gitpod.io/#https://gitlab.com/theiax/custom-ide-dev" target="_blank" rel="noopener noreferrer">
  <img
    src="https://img.shields.io/badge/Contribute%20with-Gitpod-908a85?logo=gitpod"
    alt="Contribute with Gitpod"
  />
</a>

**Environment Setup**

Gitpod dev environment is already equipped with the prerequisites. To run in any other environment, the below requirements need to be taken care of.
1. Run the below command for theia development
`sudo apt-get install -y g++ gcc make python2.7 pkg-config libx11-dev libxkbfile-dev libsecret-1-dev`

2. For removing files except certain files
    `shopt -s extglob`    
    `rm -rf (!<file_to_be_excluded>)`

**Execution help**

https://theia-ide.org/docs/composing_applications

1. `yarn`
2. `yarn theia build`
3. `yarn theia start --plugins=local-dir:plugins`
	
